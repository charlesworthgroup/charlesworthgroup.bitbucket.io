$(function(){
	var current_location = location.href,
		users = [],
		file_name = '';

	if (current_location === 'http://www.zephyr.nsysu.edu.tw/members/teacher.php') {
		current_location = current_location.replace('teacher.php', '');

		file_name = 'www.zephyr.nsysu.edu.tw__國立中山大學';

		$('.teacher_list .pic').each(function(){
			var link = current_location + $(this).find('a').attr('href');

			$.ajax({
				url: link,
				async: false,
				success: function(response) { 
					try {
						var $html = $(response),
							user = {
								university: '國立中山大學',
								college: '文 學 院',
								department: '外國語文學系'
							};

						$html.find('#content2 tr').each(function(){
							var $self = $(this),
								$th = $self.find('th'),
								$td = $self.find('td:first'),
								header = $th.text().trim(),
								value = $td.text().trim();

							if (header === 'Job title') {
								user.job_name = value;
							} else if (header === 'Name') {
								user.name = value;
							} else if (header === 'Discipline') {
								var subjects = value.split('\n')

								for (var i = 0; i < subjects.length; ++i) {
									var sub = subjects[i];

									sub = sub.substr(sub.indexOf(' ') + 1);
									sub = sub.substr(0, sub.indexOf('('));

									subjects[i] = sub.trim();
								}

								user.subjects = subjects;
							} else if (header === 'E-mail') {
								user.email = decrypt_email($td.find('script').text());
							} else if (header === 'Office phone number') {
								user.phone = value;
							}
						});

						users.push(user);
					} catch (e) { 
						console.log(e);
					}
				}
			})
		});
	} else if (current_location === 'http://www.chinese.nsysu.edu.tw/page3/pages.php?ID=page301') {
        file_name = 'www.chinese.nsysu.edu.tw__國立中山大學';

        $('#editor > table:first tbody a').each(function(){
            var link = $(this).attr('href');

            $.ajax({
                url: link,
                async: false,
                success: function(response) {
                    try {
                        var $html = $(response),
                            user = {
                                university: '國立中山大學',
                                college: '文 學 院',
                                department: '中國文學系'
                            };

                        $html.find('#content2 tr').each(function(){
                            var $self = $(this),
                                $th = $self.find('th'),
                                $td = $self.find('td:first'),
                                header = $th.text().trim(),
                                value = $td.text().trim();

                            if (header === 'Title') {
                                user.job_name = value;
                            } else if (header === 'Name') {
                                user.name = value;
                            } else if (header === 'Discipline') {
                                user.subjects = value.split(',');
                            } else if (header === 'E-mail') {
                                user.email = decrypt_email($td.find('script').text());
                            } else if (header === '聯絡電話(分機)') {
                                user.phone = value;
                            }
                        });

                        users.push(user);
                    } catch (e) {
                        console.log(e);
                    }
                }
            })
        });
	} else if (current_location === 'http://etc.nsysu.edu.tw/files/11-1221-14708.php?Lang=zh-tw') {
        current_location = current_location.replace('teacher.php', '');

        file_name = 'etc.nsysu.edu.tw__國立中山大學';

        $('.ptcontent table table tr td').each(function(){
            var link = $(this).find('a').attr('href');

            $.ajax({
                url: link,
                async: false,
                success: function(response) {
                    try {
                        var $html = $(response),
                            user = {
                                university: '國立中山大學',
                                college: '文 學 院',
                                department: '英語文教學中心'
                            };

                        $html.find('#content2 tr').each(function(){
                            var $self = $(this),
                                $th = $self.find('th'),
                                $td = $self.find('td:first'),
                                header = $th.text().trim(),
                                value = $td.text().trim();

                            if (header === 'Job title') {
                                user.job_name = value;
                            } else if (header === 'Name') {
                                user.name = value;
                            } else if (header === 'Discipline') {
                                var subjects = value.split('\n');

                                for (var i = 0; i < subjects.length; ++i) {
                                    var sub = subjects[i];

                                    sub = sub.substr(sub.indexOf(' ') + 1);
                                    sub = sub.substr(0, sub.indexOf('('));

                                    subjects[i] = sub.trim();
                                }

                                user.subjects = subjects;
                            } else if (header === 'E-mail') {
                                user.email = decrypt_email($td.find('script').text());
                            } else if (header === 'Office phone number') {
                                user.phone = value;
                            }
                        });

                        users.push(user);
                    } catch (e) {
                        console.log(e);
                    }
                }
            })
        });
	}

    if (users.length > 0) {
        download(file_name + '.csv', prepare_csv(users));
    }
});

function prepare_csv(array) {
    var text = 'University;User;College;Department;Email;Phone;Subject\n';

    array.forEach(function(item){
        var subjects = item.subjects;

        if (!subjects) {
            subjects = [];
        }

        text += item.university + ';' + item.name + ';' +
            item.college + ';' + item.department + ';' +
            (item.email || '') + ';' + (item.phone || '') + ';' + subjects.join(', ') + '\n';
    });

    return text;
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function decrypt_email(text) {
    var from = text.indexOf('(\'') + 2,
        len = text.length - from - 3,
        doc_write = 'document.write(\'';

    text = unescape(text.substr(from, len));

    from = doc_write.length;
    len = text.length - from - 3;
    text = text.substr(from, len);

    return $(text).text().trim();
}